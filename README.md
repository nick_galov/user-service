Task 1

- admin server configured and enabled on http://localhost:8282/#/applications

- eureka enable on http://localhost:8761/

- hc actuator enabled on http://localhost:8080/actuator

        e.g. http://localhost:8080/actuator/beans e. t. c.

- as DB has been used H2 in-mem-db available on h2 - console admin via http://localhost:8080/h2-console

            username: sa
            password: sa
            driver-class-name: org.h2.Driver
            url: jdbc:h2:mem:db

- settings are provided from Cloud Settings available on http://localhost:8888/user-service/default

            propertySources: [
                {
                    name: "classpath:configs/user-service/user-service-default.yml",
                    source: {
                        spring.datasource.username: "sa",
                        spring.datasource.password: "sa",
                        spring.datasource.driver-class-name: "org.h2.Driver",
                        spring.datasource.url: "jdbc:h2:mem:db;DB_CLOSE_DELAY=-1",
                        spring.boot.admin.url: "http://localhost:8282",
                        spring.management.security.enabled: false,
                        spring.cloud.stream.bindings.input.group: "test",
                        spring.cloud.stream.bindings.input.destination: "queue.pretty.log.messages",
                        spring.cloud.stream.bindings.input.binder: "local_rabbit_1",
                        spring.cloud.stream.bindings.output.group: "test",
                        spring.cloud.stream.bindings.output.destination: "queue.log.messages",
                        spring.cloud.stream.bindings.output.binder: "local_rabbit",
                        spring.cloud.stream.default-binder: "local_rabbit",
                        spring.cloud.stream.rabbit.bindings.input.consumer.enableDlq: true,
                        spring.cloud.stream.rabbit.bindings.input.consumer.dlqName: "dead-out",
                        spring.cloud.stream.rabbit.bindings.input.consumer.autoCommitOnError: true,
                        spring.cloud.stream.rabbit.bindings.input.consumer.autoCommitOffset: true,
                        spring.cloud.stream.binders.local_rabbit.type: "rabbit",
                        spring.cloud.stream.binders.local_rabbit.environment.spring.rabbitmq.host: "localhost",
                        spring.cloud.stream.binders.local_rabbit.environment.spring.rabbitmq.port: 5672,
                        spring.cloud.stream.binders.local_rabbit.environment.spring.rabbitmq.username: "guest",
                        spring.cloud.stream.binders.local_rabbit.environment.spring.rabbitmq.password: "guest",
                        spring.cloud.stream.binders.local_rabbit.environment.spring.rabbitmq.virtual-host: "/",
                        spring.cloud.stream.binders.local_rabbit_1.type: "rabbit",
                        spring.cloud.stream.binders.local_rabbit_1.environment.spring.rabbitmq.host: "localhost",
                        spring.cloud.stream.binders.local_rabbit_1.environment.spring.rabbitmq.port: 5673,
                        spring.cloud.stream.binders.local_rabbit_1.environment.spring.rabbitmq.username: "guest",
                        spring.cloud.stream.binders.local_rabbit_1.environment.spring.rabbitmq.password: "guest",
                        spring.cloud.stream.binders.local_rabbit_1.environment.spring.rabbitmq.virtual-host: "/",
                        spring.cloud.stream.rabbit.bindings.input.consumer.auto-bind-dlq: true
                    }
                }
            ]

- There are two master queues which represent consumer and producer and bound to separate ports [5672 and 5673]
            another two provides DLQ, configured in settings/

            Messages are being sent to consumer queue which is producer for side service that process and redirect
            messages to second one. That is producer for user-service. This service retrieves and log such messages.
            Message sending triggered by storing user data.

- Entities API represented on http://localhost:8080/browser/index.html#/

            Customized endpoints are :
                 GET_PERMISSIONS_BY_ID = "/{userId}/permissions";
                 ADD_PERMISSIONS_BY_IDS = "/{userId}/permissions/{roleId}";

- Task scenarios represented via controller integration tests.

- Unit tests cover Task 2 and Task 3, that have been done and placed in com.example.userservice.utils package.