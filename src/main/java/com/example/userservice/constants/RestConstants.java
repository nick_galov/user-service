package com.example.userservice.constants;

public class RestConstants {
    public static final String USER_MAPPING = "/users";
    public static final String USER_BY_ID = "/by-id/{id}";
    public static final String GET_PERMISSIONS_BY_ID = "/{userId}/permissions";
    public static final String ADD_PERMISSIONS_BY_IDS = "/{userId}/permissions/{roleId}";
    public static final String USER_ID = "userId";
    public static final String ROLE_ID = "roleId";
}
