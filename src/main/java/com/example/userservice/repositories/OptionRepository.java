package com.example.userservice.repositories;

import com.example.userservice.pojos.roles.Option;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OptionRepository extends JpaRepository<Option, String> {
}
