package com.example.userservice.repositories;

import com.example.userservice.pojos.roles.ExtendedUserRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExtendedRoleRepository extends JpaRepository<ExtendedUserRole, String> {
}
