package com.example.userservice.repositories;

import com.example.userservice.pojos.roles.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<UserRole, String> {
}
