package com.example.userservice.repositories.handlers;

import com.example.userservice.messaging.MessageSender;
import com.example.userservice.pojos.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.*;
import org.springframework.stereotype.Service;

@Service
@RepositoryEventHandler(User.class)
public class RepositoryHandler {

    @Autowired
    private MessageSender messageSender;

    @HandleBeforeCreate
    @HandleBeforeSave
    @HandleAfterLinkSave
    public void sendMessage(User user) {
        messageSender.routeValues("Added new user " + user);
    }
}
