package com.example.userservice.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;

@Slf4j
public class DestinationCalculator {
    private static final double PRECISION = 0.001d;
    private static final String OUTPUT_PATTERN = "{{},{}}";
    public static final String RESULT_PATTERN = "%s,%s";

    // Ax + By + C = 0
    class ABCLine {
        double a, b, c;
        ABCLine(double a, double b, double c) {
            this.a = a / b;
            this.b = b / b;
            this.c = c / b;
        }
        public String toString() {
            return a + " * X + " + b + " * Y + " + c + " = 0";
        }

        double calculateY(double x) {
            return -(a * x + c) / b;
        }

        double calculateX(double y) {
            return -(b * y + c) / a;
        }

        double getAngleK() {
            return -a / b;
        }
    }

    private ABCLine  getABCLineByPoints(double x1, double y1, double x2, double y2) {
        return new ABCLine(y1 - y2, x2 - x1, x1 * y2 - x2 * y1);
    }

    private ABCLine  getABCLineByPointAndAngle(double x1, double y1, double k) {
        return new ABCLine(-k, 1, k * x1 - y1);
    }

    private static double  turnAngle(double angle, double delta) {
        return Math.tan(Math.atan(angle) + delta);
    }


    public String calc(double xA, double yA, double xB, double yB) {
        double xC, yC;

        ABCLine ABLine = getABCLineByPoints(xA, yA, xB, yB);
        ABCLine BCLine = getABCLineByPointAndAngle(xB, yB,
                turnAngle(ABLine.getAngleK(), Math.PI / 2));

        int direction = getDirection(xA, yA, xB, yB, BCLine);

        double step = direction * PRECISION / 2;

        xC = xB + 3 * step;
        yC = BCLine.calculateY(xC);

        while (!areInts(xC, yC)) {
            xC += step;
            yC = BCLine.calculateY(xC);
        }
        log.info(OUTPUT_PATTERN, Math.round(xC), Math.round(yC));
        return String.format(RESULT_PATTERN, Math.round(xC), Math.round(yC));
    }

    private static int getDirection(double xA, double yA, double xB, double yB, ABCLine targetLine) {
        double xC = xB + 10;
        double yC = targetLine.calculateY(xC);
        double angle = ((xA - xB)*(yC - yB) - (xC - xB)*(yA - yB))
                / (Math.sqrt(Math.pow((xA - xB), 2) + Math.pow((yA - yB), 2))
                    * Math.sqrt(Math.pow((xC - xB), 2) + Math.pow((yC - yB), 2)));
        return angle > 0 ? 1 : -1;
    }

    private static boolean areInts(Double ... args) {
        return Arrays.stream(args).allMatch(DestinationCalculator::isNearInt);
    }

    private static boolean isNearInt(Double d) {
        return Math.abs(d - Math.round(d)) < PRECISION;
    }
}
