package com.example.userservice.utils;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ArrayUtils;

import java.util.Arrays;
import java.util.LinkedList;

@Slf4j
@Getter
public class AccountEncoder {
    private static final String RESULT_PATTERN = "result : {}";
    public static final int MAX_LIMIT_BOUND = 100_000_000;
    public static final int MIN_LIMIT_BOUND = 0;
    private static final String BOUND_ERROR_MSG =
            "numberl must be between " + MIN_LIMIT_BOUND + " and " + MAX_LIMIT_BOUND;

    public static int getEncodedNumber(int number) {
        if (number > MAX_LIMIT_BOUND || number < MIN_LIMIT_BOUND)
            throw new IllegalArgumentException(BOUND_ERROR_MSG);
        String s = String.valueOf(number);
        StringBuilder result = new StringBuilder();
        LinkedList<Character> deque = new LinkedList<>();
        deque.addAll(Arrays.asList(ArrayUtils.toObject(s.toCharArray())));
        while (!deque.isEmpty()) {
            result.append(deque.removeFirst());
            if (deque.isEmpty())
                break;
            result.append(deque.removeLast());
        }
        return Integer.valueOf(result.toString());
    }
}
