package com.example.userservice.pojos.roles;

public enum RoleType {
    USER,
    ADMIN,
    ANONYMOUS
}
