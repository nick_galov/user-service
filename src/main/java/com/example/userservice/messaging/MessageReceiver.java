package com.example.userservice.messaging;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@EnableBinding(MyProcessor.class)
public class MessageReceiver {

    @StreamListener(MyProcessor.INPUT)
    public void routeValues(String msg) {
        log.info("received : {}", msg);
    }
}
