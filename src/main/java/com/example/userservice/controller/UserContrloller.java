package com.example.userservice.controller;


import com.example.userservice.constants.RestConstants;
import com.example.userservice.pojos.User;
import com.example.userservice.pojos.roles.UserRole;
import com.example.userservice.repositories.ExtendedRoleRepository;
import com.example.userservice.repositories.RoleRepository;
import com.example.userservice.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

import static com.example.userservice.constants.RestConstants.USER_MAPPING;

@RestController
@Slf4j
@RequestMapping(USER_MAPPING)
public class UserContrloller {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ExtendedRoleRepository extendedRoleRepository;

    @GetMapping(RestConstants.GET_PERMISSIONS_BY_ID)
    public ResponseEntity<User> getUserWithOptions(
            @PathVariable String userId,
            @RequestParam(required = false) boolean showOptions) {
        Optional<User> user = Optional.of(userRepository.getOne(userId));;
        return ResponseEntity.ok(user.get());
    }

    @PostMapping(RestConstants.ADD_PERMISSIONS_BY_IDS)
    public ResponseEntity<User> addRole(
            @PathVariable String userId,
            @PathVariable String roleId,
            @RequestParam(required = false) boolean extended) {
        Optional<User> user = Optional.of(userRepository.getOne(userId));;
        Optional<UserRole> userRole;
        if(extended) {
            userRole = Optional.of(extendedRoleRepository.getOne(roleId));
        } else {
            userRole = Optional.of(roleRepository.getOne(roleId));
        }
        user.get().getRoleSet().add(userRole.get());
        userRepository.save(user.get());
        return ResponseEntity.ok(user.get());
    }

}
