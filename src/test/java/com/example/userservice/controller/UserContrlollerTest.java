package com.example.userservice.controller;

import com.example.userservice.constants.RestConstants;
import com.example.userservice.messaging.MessageSender;
import com.example.userservice.pojos.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = UserContrlollerTestConfigurations.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserContrlollerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private MessageSender messageSender;

    @Test
    public void getUserById() throws Exception {
        Map<String, String> variables = new HashMap<>();
        variables.put(RestConstants.USER_ID, "testUserId");
        variables.put(RestConstants.ROLE_ID, "testRoleId");
        String targetUrl = UriComponentsBuilder.fromUriString(
                RestConstants.ADD_PERMISSIONS_BY_IDS).buildAndExpand(variables).toUriString();
        mockMvc.perform(get(RestConstants.USER_MAPPING + "/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("ivan")));
    }

    @Test
    public void getUserByIdAdvanced() throws Exception {
        Map<String, String> variables = new HashMap<>();
        variables.put(RestConstants.USER_ID, "1");
        String targetUrl = UriComponentsBuilder.fromUriString(
                RestConstants.GET_PERMISSIONS_BY_ID).buildAndExpand(variables).toUriString();
        System.out.println("urlllll " + targetUrl);
        mockMvc.perform(get(RestConstants.USER_MAPPING + targetUrl))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("ivan")));
    }

    @Test
    public void getUser() throws Exception {
        String testName = "Petya";
        User user = new User();
        user.setName(testName);
        ObjectMapper mapper = new ObjectMapper();
        String jsonUser = mapper.writeValueAsString(user);
        String location = mockMvc.perform(post(RestConstants.USER_MAPPING)
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content(jsonUser))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getHeader("location");
        mockMvc.perform(get(location))
                .andExpect(jsonPath("$.name", is(testName)));
        verify(messageSender, times(1))
                .routeValues(eq("Added new user User(id=null, name=Petya, roleSet=[])"));
    }
}