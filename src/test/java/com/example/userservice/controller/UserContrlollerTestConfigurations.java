package com.example.userservice.controller;

import com.example.userservice.messaging.MessageSender;
import org.h2.tools.Server;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.sql.SQLException;

@Configuration
@ComponentScan("com.example.userservice")
public class UserContrlollerTestConfigurations {
    @MockBean
    public MessageSender handler;
}
