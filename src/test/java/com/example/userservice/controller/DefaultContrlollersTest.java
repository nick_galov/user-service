package com.example.userservice.controller;

import com.example.userservice.UserServiceApplication;
import com.example.userservice.constants.RestConstants;
import com.example.userservice.repositories.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = UserContrlollerTestConfigurations.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DefaultContrlollersTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getAllUsers() throws Exception {
        AntPathMatcher pathMatcher = new AntPathMatcher();
        Map<String, String> variables = new HashMap<>();
        variables.put(RestConstants.USER_ID, "testUserId");
        variables.put(RestConstants.ROLE_ID, "testRoleId");
        String targetUrl = UriComponentsBuilder.fromUriString(
                RestConstants.ADD_PERMISSIONS_BY_IDS).buildAndExpand(variables).toUriString();

        mockMvc.perform(
                get(RestConstants.USER_MAPPING))
                .andExpect(status().isOk());
    }
}