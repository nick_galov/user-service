package com.example.userservice.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DestinationCalculatorTest {

    @Test
    public void main() {
        DestinationCalculator destinationCalculator = new DestinationCalculator();
        double xA, yA, xB, yB, xC, yC;
        xA = -1;
        yA = 3;

        xB = 3;
        yB = 1;

        xC = 2;
        yC = -1;

        String expected = String.format(DestinationCalculator.RESULT_PATTERN, Math.round(xC), Math.round(yC));
        String actual = destinationCalculator.calc(xA, yA, xB, yB);

        assertEquals(expected, actual);
    }
}