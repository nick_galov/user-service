package com.example.userservice.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AccountEncoderTest {

    @Test
    public void testGetDecodedNumberOdd() {
        int testValue = 123456;
        int expected = 162534;
        int actual = AccountEncoder.getEncodedNumber(testValue);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetDecodedNumberEval() {
        int testValue = 1234567;
        int expected = 1726354;
        int actual = AccountEncoder.getEncodedNumber(testValue);
        assertEquals(expected, actual);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetDecodedNumberFailGreaterThenMax() {
        int testValue = AccountEncoder.MAX_LIMIT_BOUND + 1;
        AccountEncoder.getEncodedNumber(testValue);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetDecodedNumberFailLessThenMin() {
        int testValue = AccountEncoder.MIN_LIMIT_BOUND -1;
        AccountEncoder.getEncodedNumber(testValue);
    }
}